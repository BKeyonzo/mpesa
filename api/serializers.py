from rest_framework import serializers
from .models import Trail

class TrailSerializer(serializers.ModelSerializer):
    class Meta:
        """Profile serializer """
        model = Trail
        fields = '__all__'