# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
class Trail(models.Model):
    name = models.CharField(max_length=15, unique=True)

class MpesaRequest(models.Model):
    checkout_request_id = models.IntegerField()
    status = models.CharField(max_length=15)
